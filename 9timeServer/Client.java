import java.net.*;
public class Client {
	public static void main(String[] args) {
		try {
			DatagramSocket ds = new DatagramSocket();
			DatagramPacket sent, received;
			byte buf[] = new byte[100];
			byte buf2[] = new byte[100];
			String time;
			InetAddress ip = InetAddress.getLocalHost();
			received = new DatagramPacket (buf, buf.length);
			sent = new DatagramPacket (buf2, buf2.length, ip, 1234);
			try {
				// make request to server for time
				ds.send(sent);
				// receive time
				ds.receive(received);
			} catch(Exception e){
				e.printStackTrace();
			}
			time = new String (received.getData());
			System.out.println("Time : " + time);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
