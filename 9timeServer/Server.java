import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.*;
public class Server {
	public static void main(String[] args) {
		try {
			DatagramSocket ds = new DatagramSocket (1234);
			System.out.println("Server is up\n");
			byte[] buf = new byte[100];
			byte[] buf2 = new byte[100];
			Date date;
			String time;
			DatagramPacket sent, received;
			InetAddress ip;
			int port;
			while (true) {
				// A request from client is needed to get the client's ip, to which time is to be sent
				received = new DatagramPacket(buf2, buf2.length);
				ds.receive(received);
				ip = received.getAddress();
				port = received.getPort();
				date = new Date();
				time = date + "";
				buf = time.getBytes();
				sent = new DatagramPacket(buf, buf.length, ip, port);
				ds.send(sent);
			}
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
