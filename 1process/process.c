#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
int
main () {
	pid_t pid;

	//fork
	pid = fork ();
	if (pid < 0) {
		//error
		fprintf (stderr, "fork failed");
		return 1;
	} else if (pid == 0) {
		//child
		printf ("Child with pid %d executing ls command..\n\n", getpid());
		execlp ("/bin/ls", "ls", NULL);
	} else {
		//parent
		//wait for child to complete
		wait (NULL);
		printf ("Child Complete\n");
	}
	return 0;
}
