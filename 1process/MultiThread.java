class MultiThreadingDemo implements Runnable
{
	public void run ()
	{
		try {
			//display thread
			System.out.println ("Thread " + Thread.currentThread().getId() + " is running");
		} catch (Exception e) {
			System.out.println ("Exception is caught");
		}
	}
}
class MultiThread
{
	public static void main (String args[])
	{
		int n = 4; //number of threads
		for (int i = 0; i < n; i++) {
			Thread obj = new Thread (new MultiThreadingDemo());
			obj.start();
		}
	}
}
