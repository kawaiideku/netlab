#include <stdio.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#define MAX 20
//globals
int read_count = 0;
sem_t rw_mutex, read_count_mutex;

//prototypes
void* reader ();
void* writer ();

void*
reader (int *id)
{
	do {
		sem_wait (&read_count_mutex);
		read_count++;
		if (read_count == 1)
			sem_wait (&rw_mutex);
		sem_post (&read_count_mutex);
		printf ("Reading is performed by reader %d\n", *id);
		sem_wait (&read_count_mutex);
		read_count--;
		if (read_count == 0)
			sem_post (&rw_mutex);
		sem_post (&read_count_mutex);
		sleep (2);
	} while (1);
}
void*
writer (int *id)
{
	do {
		sem_wait (&rw_mutex);
		printf ("Writing is performed by writer %d\n", *id);
		sem_post (&rw_mutex);
		sleep (2);
	} while (1);
}

int
main ()
{
	int i, reader_n = 2, writer_n = 2, reader_ids[MAX], writer_ids[MAX];
	pthread_t readers[MAX], writers[MAX];
	sem_init (&rw_mutex, 1, 1);
	sem_init (&read_count_mutex, 1, 1);

	for (i = 0; i < reader_n; i++) {
		reader_ids[i] = i;
		pthread_create (&readers[i], NULL, reader, &reader_ids[i]);
	}
	for (i = 0; i < writer_n; i++) {
		writer_ids[i] = i;
		pthread_create (&writers[i], NULL, writer, &writer_ids[i]);
	}
	
	//wait for threads to terminate
	for (i = 0; i < reader_n; i++)
		pthread_join (readers[i], NULL);
	for (i = 0; i < writer_n; i++)
		pthread_join (writers[i], NULL);
	return 0;
}
