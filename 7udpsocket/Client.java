import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.net.SocketException;

class Client {
	public static void main (String args[]) {
		Scanner sc = new Scanner (System.in);
		try {
			DatagramSocket ds = new DatagramSocket();
			InetAddress ip = InetAddress.getLocalHost();
			byte buf[] = null;

			while (true) {
				String input = sc.nextLine();
				buf = input.getBytes();
				DatagramPacket dpSend = new DatagramPacket (buf, buf.length, ip, 1234);
				ds.send (dpSend);
				if (input.equals ("Over"))
					break;
			}
		} catch (SocketException se) {
			System.out.println (se);
		} catch (UnknownHostException uhe) {
			System.out.println (uhe);
		} catch (IOException ie) {
			System.out.println (ie);
		}
	}
}
