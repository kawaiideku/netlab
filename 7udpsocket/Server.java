import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

class Server {
	public static void main (String args[]) {
		try {
			DatagramSocket ds = new DatagramSocket (1234);
			byte[] buf = new byte[65535];
			DatagramPacket dpReceive = null;
			while (true) {
				dpReceive = new DatagramPacket (buf, buf.length);
				try {
					ds.receive (dpReceive);
				} catch (IOException ie) {
					System.out.println (ie);
				}
				System.out.println ("Client : " + data (buf));
				if (data (buf).toString ().equals ("Over")) {
					System.out.println ("Client says Over");
					break;
				}
				buf = new byte[65535];
			}

		} catch (SocketException se) {
			System.out.println (se);
		}
	}

	public static StringBuilder data (byte[] a) {
		if (a == null)
			return null;
		StringBuilder ret = new StringBuilder ();
		int i = 0;
		while (a[i] != 0) {
			ret.append ((char) a[i]);
			i++;
		}
		return ret;
	}
}
