import java.net.*;
import java.io.*;
//class to handle messages to client
class FromClient implements Runnable {
	int max;
	int id;
	PrintWriter serverOuts[]; 
	String inLine = "";
	Socket socket = null;
	BufferedReader clientIn;
	FromClient (Socket socket, PrintWriter serverOuts[],int max, int id) {
		this.socket = socket;
		this.serverOuts = serverOuts;
		this.max = max;
		this.id = id;
		try {
			clientIn = new BufferedReader(
					new InputStreamReader(socket.getInputStream()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void run () {
		while (inLine != "Over" || inLine != null) {
			try {
				inLine = clientIn.readLine();
				System.out.println(inLine);
				//broadcast
				for (int i = 0; i < max; i++) {
					if (serverOuts[i] == null || i == id)
						continue;
					serverOuts[i].println(inLine);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
		try {
			System.out.println ("Closing connection");
			socket.close();
			clientIn.close();
			for (int i = 0; i < max; i++) {
				if (serverOuts[i] == null)
					continue;
				serverOuts[i].close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

class Server {
	public static void main(String[] args) {
		//array of sockets for broadcast to clients
		int max = 10;
		// Socket sockets[] = new Socket[max];
		Socket socket;
		ServerSocket server = null;
		PrintWriter serverOuts[] = new PrintWriter[max];
		try {
			server = new ServerSocket(1234);
			System.out.println ("Server started\nWaiting for clients...");
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (int i = 0; i < max; i++) {
			try {
				socket = server.accept();
				System.out.println ("Client " + i + " connected");
				serverOuts[i] = new PrintWriter(socket.getOutputStream(), true);
				
				new Thread(new FromClient(socket, serverOuts, max, i)).start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
