import java.net.*;
import java.io.*;
//class to handle messages FROM server
class FromServer implements Runnable {
	Socket socket = null;
	BufferedReader in;
	String inLine = "";
	//need same socket from Client object
	FromServer (Socket socket) {
		this.socket = socket;
		try {
			in = new BufferedReader(
					new InputStreamReader(socket.getInputStream()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void run() {
		while (inLine != "Over" || inLine != null) {
			try {
				inLine = in.readLine();
				System.out.println(inLine);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//close
		try {
			socket.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
//connects to server and sends input from stdin to server
class Client {
	public static void main (String args[]) {
		Socket socket = null;
		PrintWriter clientOut = null;
		BufferedReader br = null;
		String name = "";
		String outLine = "";
		try {
			socket = new Socket ("127.0.0.1", 1234);
			System.out.println ("connected");

			clientOut = new PrintWriter(socket.getOutputStream(), true);
			br = new BufferedReader(
					new InputStreamReader(System.in));
		
			System.out.print ("Enter your name : ");
			name = br.readLine();
			System.out.println ("\nEnter messages : \n");
			// start thread to receive msgs from others (via server)
			FromServer fromServer = new FromServer(socket);
			Thread t2 = new Thread (fromServer);
			t2.start();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		//keep reading till Over is input
		while (outLine != "Over" && outLine != null) {
			try {
				outLine = br.readLine();
				clientOut.println(name + " : " + outLine);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			clientOut.close();
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
