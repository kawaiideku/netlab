import java.io.IOException; 
import org.apache.commons.net.ftp.FTPClient; 
class Client { 
	public static void main(String args[]) 
	{ 
		FTPClient ftp = new FTPClient(); 
		try { 
			ftp.connect("127.0.0.1",2121); 
			boolean isSuccess = ftp.login("anonymous",""); 

			if (isSuccess) { 
				String[] filesFTP = ftp.listNames(); 
				int count = 1; 
				for (String file : filesFTP) { 
					System.out.println("File " + count + " :" + file); 
					count++; 
				} 
			} 
			ftp.logout(); 
		} 
		catch (IOException e) { 
			e.printStackTrace(); 
		} 
		finally { 
			try { 
				ftp.disconnect(); 
			} 
			catch (IOException e) { 
				e.printStackTrace(); 
			} 
		} 
	} 
} 

