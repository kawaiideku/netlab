import java.net.*;
import java.io.*;
class Client {
	private Socket socket = null;
	private DataInputStream input = null;
	private DataOutputStream out = null;

	Client (String address, int port) {
		try {
			socket = new Socket (address, port);
			System.out.println ("connected");

			input = new DataInputStream (System.in);
			out = new DataOutputStream (socket.getOutputStream ());
		} catch (UnknownHostException u) {
			System.out.println (u);
		} catch (IOException i) {
			System.out.println (i);
		}
		//keep reading till Over is input
		String line = "";
		while (!line.equals ("Over")) {
			try {
				line = input.readLine();
				out.writeUTF (line);
			} catch (IOException i) {
				System.out.println (i);
			}
		}
		//close
		try {
			input.close();
			out.close();
			socket.close();
		} catch (IOException i) {
			System.out.println (i);
		}
	}
	public static void main (String args[]) {
		Client client = new Client ("127.0.0.1", 5000);
	}
}
